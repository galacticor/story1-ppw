from django.test import TestCase, Client
from django.urls import resolve
from django.http import JsonResponse
from .apps import Story9Config
from .views import *

class TestGeneral(TestCase):
	def setUp(self):
		user = User.objects.create_user(username='a@a.com', email='a@a.com', password='password', first_name='name')

	def test_app_name(self):
		self.assertEqual(Story9Config.name, "story9")

	def test_event_url_index_is_exist(self):
		response = Client().get('/story9/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/story9/')
		self.assertEqual(found.func, index)

	def test_event_using_template(self):
		response = Client().get('/story9/')
		self.assertTemplateUsed(response, 'story9.html')

	def test_url_post_registration_exist(self):
		data = {
			'name' : "Test",
			'email' : 'test@test.com',
			'password' : 'password'
		}
		response = Client().post('/story9/register/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_url_registration_already_exist(self):
		data = {
			'name' : "Test",
			'email' : 'a@a.com',
			'password' : 'password'
		}
		response = Client().post('/story9/register/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_login_success(self):
		data = {
			'email' : "a@a.com",
			'password' : 'password'
		}
		response = Client().post('/story9/login/', data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_login_failed(self):
		data = {
			'email' : "a@a.com",
			'password' : 'passwords'
		}
		response = Client().post('/story9/login/', data)
		self.assertEqual(response.status_code, 302)

	def test_url_post_logout_exist(self):
		response = Client().post('/story9/logout/')
		self.assertEqual(response.status_code, 302)

	def test_func_url_logout(self):
		found = resolve('/story9/logout/')
		self.assertEqual(found.func, logout)

	def test_func_url_registration(self):
		found = resolve('/story9/register/')
		self.assertEqual(found.func, register)
		
	def test_func_url_login(self):
		found = resolve('/story9/login/')
		self.assertEqual(found.func, login)



# Create your tests here.
