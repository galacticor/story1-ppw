from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth

# Create your views here.
def index(request) :
	msg = request.session.get('msg', None)
	try:
		del request.session['msg']
	except:
		pass

	context = {
		'msg' : msg,
	}
	return render(request, 'story9.html', context)

def register(request):
    if request.method == 'POST':
        name = request.POST['name']
        email = request.POST['email']
       	password = request.POST['password']

       	hasil = "Registration Complete"
       	if User.objects.filter(username=email).exists() :
       		hasil = "Email already exist, try login instead"
       	else :
       		user = User.objects.create_user(username=email, email=email, password=password, first_name=name)
       		user.save()
        	hasil = "Registration Complete, now you may Login"

        request.session['msg'] = hasil
        

    return redirect('/story9/')

def login(request):
    if request.method == 'POST':
        username = request.POST['email']
        password = request.POST['password']

        user = auth.authenticate(username=username, password=password)

        hasil = ''
        if user is not None:
            auth.login(request, user)
            request.session.set_expiry(300) # auto logout dalam 5 menit
            hasil = 'Login success'
        else :
        	hasil = "Wrong Username or Password"

        request.session['msg'] = hasil
        
    
    return redirect('/story9/')

def logout(request):
    auth.logout(request)
    return redirect("../")
