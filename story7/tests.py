from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .apps import Story7Config
from .views import *

class TestGeneral(TestCase):
	def test_app_name(self):
		self.assertEqual(Story7Config.name, "story7")

	def test_event_url_is_exist(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/story7/')
		self.assertEqual(found.func, index)

	def test_event_using_template(self):
		response = Client().get('/story7/')
		self.assertTemplateUsed(response, 'story7.html')

# Create your tests here.
