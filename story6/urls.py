from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.kegiatan, name="index"),
    path('<int:id>/register/', views.register, name='register'),
    path('peserta/<int:id>/delete/', views.hapus_peserta, name='hapus_peserta'),
]