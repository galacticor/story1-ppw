from django.db import models

# Create your models here.
class Kegiatan(models.Model):
	nama = models.CharField(max_length=120, unique=True, error_messages={'unique':"Kegiatan dengan nama ini sudah ada"})
	desc = models.TextField(blank=True, null=True, default="Kuyy")
	jml_peserta = models.PositiveIntegerField(default=0)

	class Meta:
		ordering = ['nama']

	# def __str__(self):
	# 	return "{} - {}".format(self.id, self.nama)


class Peserta(models.Model):
	nama = models.CharField(max_length=120)
	kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE,  null=True, blank=True)

	class Meta:
		ordering = ['nama']

	# def __str__(self):
	# 	return "{} - {}".format(self.id, self.nama)