from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import *

class TestKegiatan(TestCase):
	def test_event_url_is_exist(self):
		response = Client().get('/kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/kegiatan/')
		self.assertEqual(found.func, kegiatan)

	def test_event_using_template(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'kegiatan.html')


class TestTambahKegiatan(TestCase):
	def test_add_event_url_is_exist(self):
		response = Client().get('/kegiatan/')
		self.assertEqual(response.status_code, 200)

	def test_add_event_index_func(self):
		found = resolve('/kegiatan/')
		self.assertEqual(found.func, kegiatan)

	def test_add_event_using_template(self):
		response = Client().get('/kegiatan/')
		self.assertTemplateUsed(response, 'kegiatan.html')

	def test_event_model_create_new_object(self):
		acara = Kegiatan(nama="abc")
		acara.save()
		self.assertEqual(Kegiatan.objects.all().count(), 1)

	def test_event_url_post_is_exist(self):
		response = Client().post('/kegiatan/', data={'nama':'berenang'})
		self.assertEqual(response.status_code, 302)

class TestRegister(TestCase):
	def setUp(self):
		acara = Kegiatan(nama="abc")
		acara.save()

	def test_regist_url_post_is_exist(self):
		response = Client().post('/kegiatan/1/register/', data={'nama':'widy'})
		self.assertEqual(response.status_code, 302)
	
	def test_event_model_create_new_object(self):
		peserta = Peserta(nama="widy")
		peserta.save()
		self.assertEqual(Peserta.objects.all().count(), 1)

	def test_register_url_post_redirect_is_exist(self):
		response = self.client.post('/kegiatan/1/register', follow = True)
		self.assertRedirects(response, '/kegiatan/', status_code=301, 
        target_status_code=200, fetch_redirect_response=True)

class TestHapusNama(TestCase):
	def setUp(self):
		acara = Kegiatan(nama="abc")
		acara.save()
		peserta = Peserta(nama="widy")
		peserta.save()

	def test_hapus_url_post_is_exist(self):
		response = self.client.post('/kegiatan/peserta/1/delete')
		self.assertEqual(response.status_code, 301)
	
	def test_hapus_url_post_redirect_is_exist(self):
		response = self.client.post('/kegiatan/peserta/1/delete', follow = True)
		self.assertRedirects(response, '/kegiatan/', status_code=301, 
        target_status_code=200, fetch_redirect_response=True)

	def test_event_model_create_new_object(self):
		peserta = Peserta.objects.filter(nama="widy").first()
		peserta.delete()
		self.assertEqual(Peserta.objects.all().count(), 0)




