from django import forms
from .models import *

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama', 'desc']
        labels = {
            'nama': 'Nama Kegiatan',
            'desc' : 'Deskripsi',
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama']
        labels = {
            'nama': 'Nama Peserta',
        }
