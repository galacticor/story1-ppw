from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import *
from .forms import *
# Create your views here.

def kegiatan(request):
	cek=False
	form = KegiatanForm(request.POST or None)
	formP = PesertaForm()
	if form.is_valid():
		form.save()
		form = KegiatanForm()
		return redirect("/kegiatan")
	if request.POST:
		cek=True

	kegiatan = Kegiatan.objects.all()
	peserta = Peserta.objects.all()
	data = {
		'form' : form,
		'kegiatan' : kegiatan,
		'cek' : cek,
		'peserta':peserta,
		'formp' : formP,
	}	
	return render(request, 'kegiatan.html', data)

def register(request, id):
	form = PesertaForm(request.POST)
	if form.is_valid():
		pesertaBaru = Peserta(kegiatan=Kegiatan.objects.get(id=id), nama=form.data['nama'])
		pesertaBaru.save()
	
	return redirect('/kegiatan/')

def hapus_peserta(request, id):
	if request.POST:
		obj_temp = get_object_or_404(Peserta, id=id) #mengambilnamaobject
		obj_temp.delete()

	return redirect('/kegiatan/')


