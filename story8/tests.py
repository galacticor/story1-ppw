from django.test import TestCase, Client
from django.urls import resolve
from django.http import JsonResponse
from .apps import Story8Config
from .views import *

class TestGeneral(TestCase):
	def test_app_name(self):
		self.assertEqual(Story8Config.name, "story8")

	def test_event_url_is_exist(self):
		response = Client().get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_event_index_func(self):
		found = resolve('/story8/')
		self.assertEqual(found.func, index)

	def test_event_using_template(self):
		response = Client().get('/story8/')
		self.assertTemplateUsed(response, 'story8.html')

	def test_url_search_is_exist(self):
		response = Client().get('/story8/search/')
		self.assertEqual(response.status_code, 200)

	def test_response_search_type_json(self):
		response = Client().get('/story8/search/?q=test')
		if isinstance(response, JsonResponse):
			self.assertEqual(1, 1)
		else:
			self.assertEqual(1, 0)



# Create your tests here.
