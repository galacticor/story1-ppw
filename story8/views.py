from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.

def index(request):
	return render(request, 'story8.html')

def get_book(request):
	if request.GET:
		url = f"https://www.googleapis.com/books/v1/volumes?q={request.GET['q']}&maxResults=12"
		r = requests.get(url);
		return JsonResponse(r.json())

	return JsonResponse({'data':None})

# def create_book(request) :
#     if(request.is_ajax()) :
#         Books.objects.create(title=request.POST['title'], author=request.POST['author'], publisher=request.POST['publisher'], publishedDate=request.POST['publishedDate'], likes=0)

