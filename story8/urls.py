from django.urls import path
from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.index, name="index"),
    # path('create_book/', views.create_book, name='create_book'),
    path('search/', views.get_book, name='create_book'),
]