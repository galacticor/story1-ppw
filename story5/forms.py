from django import forms
from .models import Kuliah
# from editorjs_field.widgets import EditorJsWidget

class KuliahForm(forms.ModelForm):
    class Meta:
        model = Kuliah
        fields = ['nama','desc','jml_sks','dosen','semester']
        labels = {
            'nama': 'Nama Mata Kuliah',
            'desc': 'Deskripsi Tambahan',
            'jml_sks': 'Jumlah SKS',
            'dosen' : 'Dosen',
            'semester' : 'Semester',
        }
