from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.kuliah, name="index"),
    path('<int:id>/', views.detail_kuliah, name='detail_kuliah'),
    path('<int:id>/delete/', views.delete_kuliah, name='delete_kuliah'),
    path('edit/', views.editor, name="editor"),
]