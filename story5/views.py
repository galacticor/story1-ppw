from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .models import *
from .forms import *
# Create your views here.


def kuliah(request):
	cek=False
	form = KuliahForm(request.POST or None)
	if form.is_valid():
		form.save()
		form = KuliahForm()
		return redirect("/kuliah")
	if request.POST:
		cek=True

	kuliah = Kuliah.objects.all()
	data = {
		'form' : form,
		'kuliah' : kuliah,
		'cek' : cek,
	}	
	return render(request, 'matakuliah.html', data)

def detail_kuliah(request, id):
	kuliah = get_object_or_404(Kuliah, id=id)
	data = {
		'kuliah' : kuliah
	}
	return render(request, 'detailkuliah.html', data)

def delete_kuliah(request, id):
	kuliah = get_object_or_404(Kuliah, id=id)
	kuliah.delete()
	return redirect("/kuliah")

def editor(request):
	f = MyForm()
	return render(request, 'editor.html', {'form' : f})