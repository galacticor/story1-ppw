from django.db import models

# Create your models here.

class Kuliah(models.Model):
	nama = models.CharField(max_length=120, unique=True, error_messages={'unique':"Kuliah dengan nama ini sudah ada"})
	desc = models.TextField()
	jml_sks = models.PositiveIntegerField(default=1)
	dosen = models.CharField(max_length=120)
	semester = models.PositiveIntegerField(default=1)

	def __str__(self):
		return "{} - {}".format(self.id, self.nama)

class Tugas(models.Model):
    nama = models.CharField(max_length=120)
    deadline = models.DateField()
    matakuliah = models.ForeignKey(Kuliah, on_delete=models.CASCADE)

    class Meta:
        ordering = ['deadline']

    def __str__(self):
    	return self.nama
