$(document).ready(function() {
    // biar bisa pencet enter dari search
    $('#input_search').on('keyup', function(e) {
        if (e.which === 13 ) {
            searchBook()
            // $(this).val('')
        }
    })

    // click tombol search
    $('#button_search').on('click', function() {
        
        searchBook()
        // $('#input_search').val('')
    })
    $("#searchform").submit(function(e) {
        e.preventDefault()
        searchBook()
    })




    function searchBook() {
        $.ajax({
            url: "/story8/search/",
            type: 'get',
            dataType: 'json',
            data: {
                'q': $('#input_search').val(),
            },
            // url: 'https://www.googleapis.com/books/v1/volumes',
            // type: 'get',
            // dataType: 'json',
            // data: {
            //     'q': $('#input_search').val(),
            //     'maxResults': 20,
            // },
        success: function(data) {
                $("#booktable-content").empty()
                if(data.totalItems == 0) {
                    $("#booktable").css("display", "none")
                }
                else {
                    $("#booktable").css("display", "table")
                    var content = ""
                    for(var i = 0; i < data.items.length; i++) {
                        content += "<tr>"
                        
                        var Identifier
                        try {
                            Identifier = data.items[i].volumeInfo.industryIdentifiers[0].identifier
                        } catch(TypeError) {
                            Identifier = "Not Available"
                        } finally {
                            content += `<td class='align-middle'>${Identifier}</td>`
                        }

                        var Cover
                        try {
                            Cover = `<img src='${data.items[i].volumeInfo.imageLinks.thumbnail}'></img>`
                        } catch(TypeError) {
                            Cover = "Not Available"
                        } finally {
                            content += `<td class='align-middle'>${Cover}</td>`
                        }

                        var Id = data.items[i].id
                        var Title = data.items[i].volumeInfo.title
                        var Author = data.items[i].volumeInfo.authors
                        var Publisher = data.items[i].volumeInfo.publisher
                        var PDate = data.items[i].volumeInfo.publishedDate
                        
                        content += `<td class='align-middle'>${Title}</td>`
                        content += `<td class='align-middle'>${Author}</td>`
                        content += `<td class='align-middle'>${Publisher}</td>`
                        content += `<td class='align-middle'>${PDate}</td>`
                        
                        content += "</tr>"

                        /**
                         * <input type="hidden" name="Identifier" value="${Identifier}">
                            <input type="hidden" name="Cover" value="${Cover}">
                            <input type="hidden" name="Title" value="${Title}">
                            <input type="hidden" name="Author" value="${Author}">
                            <input type="hidden" name="Publisher" value="${Publisher}">
                            <input type="hidden" name="PDate" value="${PDate}">
                         */
                    }
                    
                    $("#booktable-content").append(content)
                }
            },
        })

    }

})
