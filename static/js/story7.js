$(document).ready(function() {
    $(".accordion_header").click(function(){
        $(".accordion_header").removeClass("active")
        $(this).addClass("active")
    });

    $('.apasi').on('click', function(){
        var parentDiv = $(this).closest('div.panel-default'),
            dir = $(this).data('sort');

        if(dir === 'up'){
            parentDiv.insertBefore( parentDiv.prev() )
        } else if(dir === 'down'){
            parentDiv.insertAfter( parentDiv.next() )
        }
    });

    $('.wrapper').sortable();
    

    $('.panel-collapse').on('show.bs.collapse', function () {
	    $(this).siblings('.panel-heading').addClass('active');
	  });

	  $('.panel-collapse').on('hide.bs.collapse', function () {
	    $(this).siblings('.panel-heading').removeClass('active');
	  });
});
	
