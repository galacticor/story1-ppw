$('#password, #confirm_password').on('keyup', function () {
	const pass1 = $('#password').val()
	const pass2 = $('#confirm_password').val()
	$('#submit-register').prop('disabled', true);
	if (pass1 != pass2) {
		$('#message').html('Password Not Match').css('color', 'red');
	} else if (pass1.length < 8){
		$('#message').html('Password min length 8').css('color', 'red');
	} else {
		$('#submit-register').prop('disabled', false);
		$('#message').html('Password Good').css('color', 'green');
	}
});